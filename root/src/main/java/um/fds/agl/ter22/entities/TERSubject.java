package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TERSubject {
    private @Id @GeneratedValue Long idTER;
    public String teacherLastName;
    public String secondTeacherLastName;
    private String title;

    public TERSubject() {
    }

    public TERSubject(String title) {
        this.title = title;
    }

    public TERSubject(String teacherLastName, String secondTeacherLastName, String title) {
        this.teacherLastName = teacherLastName;
        this.secondTeacherLastName = secondTeacherLastName;
        this.title = title;
    }

    public Long getIdTER() {
        return idTER;
    }

    public String getTeacher() {
        return this.teacherLastName;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTeacher(String teacher) {
        this.teacherLastName = teacher;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSecondTeacher() {
        return secondTeacherLastName;
    }

    public void setSecondTeacher(String secondTeacherLastName) {
        this.secondTeacherLastName = secondTeacherLastName;
    }

    @Override
    public String toString() {
        return "TERSubject{" +
                "idTER=" + idTER +
                ", teacherLastName='" + teacherLastName + '\'' +
                ", SecondTeacherLastName='" + secondTeacherLastName + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}