package seleniumTests;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class creationProfTest extends BaseForTests{

    @Test
    void echecfulcreation()  {
        login("Turing", "turing");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
        click(driver.findElement(By.cssSelector("body > a:nth-child(3)")));
        click(driver.findElement(By.cssSelector("body > a:nth-child(2)")));
        assertTrue(read(driver.findElement(By.cssSelector("body > h1:nth-child(1)"))).contains(":-("));
        click(driver.findElement(By.cssSelector("body > a:nth-child(3)")));
        click(driver.findElement(By.cssSelector("body > a:nth-child(3)")));
        boolean ok = false;
        try {
            driver.findElement(By.cssSelector("body > div:nth-child(5) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5)"));
        } catch (Exception e){
            System.out.println("EXCEPTION : "+e);
            ok = true;
        }
        assertTrue(ok);
    }

    @Test
    void successfulcreation() throws IOException {
        login("Chef", "mdp");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
        click(driver.findElement(By.cssSelector("body > a:nth-child(3)")));
        click(driver.findElement(By.cssSelector("body > a:nth-child(2)")));
        write(driver.findElement(By.cssSelector("#firstName")),"Vol");
        write(driver.findElement(By.cssSelector("#lastName")),"De mort");
        click(driver.findElement(By.cssSelector("body > form:nth-child(2) > input:nth-child(5)")));
        assertTrue(
                read(driver.findElement(By.cssSelector("body > div:nth-child(5) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(1)"))).contains("Vol") && read(driver.findElement(By.cssSelector("body > div:nth-child(5) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2)"))).contains("De mort")
        );

        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String screenshotBase64 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BASE64);
        File DestFile=new File("/home/e20200007056/Bureau/Cours/L3/Semestre 5/Génie Logiciel/Projet/projet-ter/root/src/test/java/seleniumTests/screenshot");
        FileUtils.copyFile(file, DestFile);



    }

}
