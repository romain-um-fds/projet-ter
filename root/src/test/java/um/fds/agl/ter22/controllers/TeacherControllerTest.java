package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;
import static org.hamcrest.Matchers.is;

import org.springframework.security.core.authority.AuthorityUtils;
import um.fds.agl.ter22.services.TeacherService;

import static org.assertj.core.api.Assumptions.assumeThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private TeacherService teacherService;
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        assumeThat(teacherService.getTeacher(10l).isEmpty());
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("id", "10")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        assertEquals(result.getRequest().getParameter("id"),"10");
        assertEquals(result.getRequest().getParameter("firstName"),"Anne-Marie");
        assertEquals(result.getRequest().getParameter("lastName"),"Kermarrec");
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        assumeThat(teacherService.getTeacher(10l).isEmpty());
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("id", "10")
                        .param("firstName", "Bernard")
                        .param("lastName", "Jean")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        assertEquals(result.getRequest().getParameter("id"),"10");
        assertEquals(result.getRequest().getParameter("firstName"),"Bernard");
        assertEquals(result.getRequest().getParameter("lastName"),"Jean");
        result = mvc.perform(post("/addTeacher")
                        .param("id", "10")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        assertEquals(result.getRequest().getParameter("id"),"10");
        assertEquals(result.getRequest().getParameter("firstName"),"Anne-Marie");
        assertEquals(result.getRequest().getParameter("lastName"),"Kermarrec");
    }
}