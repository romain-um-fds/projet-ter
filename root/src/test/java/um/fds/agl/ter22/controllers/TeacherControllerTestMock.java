package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.services.TeacherService;

import static com.google.common.base.Verify.verify;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTestMock {
    @Autowired
    private MockMvc mvc;
    @MockBean
    TeacherService mock1;
    @MockBean
    Teacher mock2;
    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostNonExistingTeacher() throws Exception {
        assumeThat(mock1.getTeacher(10l).isEmpty());
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("id", "10")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        Mockito.verify(mock1,atLeastOnce()).saveTeacher(mock2);
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        assumeThat(mock1.getTeacher(10l).isEmpty());
        MvcResult result = mvc.perform(post("/addTeacher")
                        .param("id", "10")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                )
                .andExpect(status().is3xxRedirection())
                .andReturn();
        Mockito.verify(mock1,atLeastOnce()).saveTeacher(mock2);
    }
}