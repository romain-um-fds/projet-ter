package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Groupe {
    private String nom;
    private @Id @GeneratedValue Long idG;

    public Groupe(){}
    public Groupe(String n){this.nom=n;}

    public String getNom(){return nom;}
    public void setNom(String n){nom=n;}
    public long getId() {return idG;}
    public void setId(Long i) {this.idG=i;}

    @Override
    public String toString() {
        return "Groupe{" +
                "nom='" + nom + '\'' +
                ", idG=" + idG +
                '}';
    }
}