package um.fds.agl.ter22.forms;

public class GroupeForm {
    private String nom;
    private long idG;

    public GroupeForm(String nom,long idG) {
        this.nom=nom;
        this.idG=idG;
    }
    public GroupeForm(){}
    public String getNom(){return nom;}
    public void setNom(String nom) {this.nom = nom;}
    public long getIdG(){return idG;}
    public void setIdG(long id){this.idG=id;}
}