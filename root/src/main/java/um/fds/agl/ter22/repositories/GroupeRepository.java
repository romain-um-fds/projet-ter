package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Groupe;

public interface GroupeRepository extends CrudRepository<Groupe, Long> {
    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    Groupe save(@Param("groupe") Groupe groupe);

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("idG") Long idG);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("groupe") Groupe groupe);
}