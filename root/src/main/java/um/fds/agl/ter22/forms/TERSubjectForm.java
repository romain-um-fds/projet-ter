package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.TERSubject;

public class TERSubjectForm {
    private long id;
    private String title;
    private String teacher1;
    private String teacher2;

    public TERSubjectForm(long id, String title, String teacher1, String teacher2) {
        this.title = title;
        this.teacher1 = teacher1;
        this.teacher2 = teacher2;
        this.id = id;
    }

    public TERSubjectForm() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTeacher1() { return teacher1; }

    public void setTeacher1(String teacher1) {
        this.teacher1 = teacher1;
    }

    public String getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(String teacher2) {
        this.teacher2 = teacher2;
    }
}
