package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.TERSubject;

public interface TERSubjectRepository extends CrudRepository<TERSubject, Long> {
    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (hasRole('ROLE_TEACHER') and (#terSubject?.teacherLastName == null or #terSubject?.teacherLastName == authentication?.name))")
    TERSubject save(@Param("terSubject") TERSubject terSubject);

    @PreAuthorize("hasRole('ROLE_MANAGER') or (hasRole('ROLE_TEACHER') and (#terSubject.findByIdTER(#idTER).get()?.teacherLastName == null or #terSubject.findByIdTER(#idTER).get()?.teacherLastName == authentication?.name))")
    void deleteById(@Param("idTER") Long idTER);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (hasRole('ROLE_TEACHER') and (#terSubject?.teacherLastName == null or #terSubject?.teacherLastName == authentication?.name))")
    void delete(@Param("terSubject") TERSubject terSubject);
}
