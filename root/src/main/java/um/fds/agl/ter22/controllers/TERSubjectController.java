package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import um.fds.agl.ter22.entities.TERSubject;
import um.fds.agl.ter22.forms.TERSubjectForm;
import um.fds.agl.ter22.services.TERService;
import um.fds.agl.ter22.services.TeacherService;

@Controller
public class TERSubjectController implements ErrorController {

    @Autowired
    private TERService terService;
    @Autowired
    private TeacherService teacherService;

    @GetMapping("/listTERSubjects")
    public Iterable<TERSubject> getTERSubjects(Model model) {
        Iterable<TERSubject> terSubjects=terService.getTERSubjects();
        model.addAttribute("terSubjects", terSubjects);
        return terSubjects;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")
    @GetMapping(value = { "/addTERSubject" })
    public String showAddTERSubjectPage(Model model) {

        TERSubjectForm terSubjectForm = new TERSubjectForm();
        model.addAttribute("terSubjectForm", terSubjectForm);
        model.addAttribute("teachers", teacherService.getTeachers());

        return "addTERSubject";
    }

    @PostMapping(value = { "/addTERSubject"})
    public String addTERSubject(Model model, @ModelAttribute("TERSubjectForm") TERSubjectForm terSubjectForm) {
        TERSubject terSubject;
        if(terService.findById(terSubjectForm.getId()).isPresent()){
            // ter already existing : update
            terSubject = terService.findById(terSubjectForm.getId()).get();
            terSubject.setTitle(terSubjectForm.getTitle());
            terSubject.setTeacher(terSubjectForm.getTeacher1());
            terSubject.setSecondTeacher(terSubjectForm.getTeacher2());
        } else {
            // ter not existing : create
            terSubject=new TERSubject(terSubjectForm.getTeacher1(), terSubjectForm.getTeacher2(), terSubjectForm.getTitle());
        }
        terService.saveTERSubject(terSubject);
        return "redirect:/listTERSubjects";

    }

    @GetMapping(value = {"/showTERSubjectUpdateForm/{id}"})
    public String showTERSubjectUpdateForm(Model model, @PathVariable(value = "id") long id){

        TERSubjectForm terSubjectForm = new TERSubjectForm(id, terService.findById(id).get().getTitle(), terService.findById(id).get().getTeacher(), terService.findById(id).get().getSecondTeacher());
        model.addAttribute("terSubjectForm", terSubjectForm);
        return "updateTERSubject";
    }

    @GetMapping(value = {"/deleteTERSubject/{id}"})
    public String deleteTERSubject(Model model, @PathVariable(value = "id") long id){
        terService.deleteTERSubject(id);
        return "redirect:/listTERSubjects";
    }

}

