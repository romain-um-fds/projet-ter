package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.TERSubject;
import um.fds.agl.ter22.repositories.StudentRepository;
import um.fds.agl.ter22.repositories.TERSubjectRepository;

import java.util.Optional;
@Service
public class TERService {
    @Autowired
    private TERSubjectRepository terSubjectRepository;

    public Optional<TERSubject> getTERSubject(final Long id) {
        return terSubjectRepository.findById(id);
    }

    public Iterable<TERSubject> getTERSubjects() {
        return terSubjectRepository.findAll();
    }

    public void deleteTERSubject(final Long id) {terSubjectRepository.deleteById(id);
    }

    public TERSubject saveTERSubject(TERSubject terSubject) {
        TERSubject savedTerSubject = terSubjectRepository.save(terSubject);
        return savedTerSubject;
    }

    public Optional<TERSubject> findById(Long id) {
        return terSubjectRepository.findById(id);
    }
}
