package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import um.fds.agl.ter22.entities.Student;
import um.fds.agl.ter22.entities.TERSubject;
import um.fds.agl.ter22.forms.StudentForm;
import um.fds.agl.ter22.services.StudentService;
import um.fds.agl.ter22.services.GroupeService;
import um.fds.agl.ter22.entities.Groupe;
import um.fds.agl.ter22.forms.GroupeForm;

@Controller
public class GroupeController implements ErrorController {
    @Autowired
    private GroupeService groupeService;

    @GetMapping("/listGroupes")
    public Iterable<Groupe> getGroupes(Model model) {
        Iterable<Groupe> groupes=groupeService.getGroupes();
        model.addAttribute("groupes", groupes);
        return groupes;
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = { "/addGroupe" })
    public String showAddGroupePage(Model model) {

        GroupeForm groupeForm = new GroupeForm();
        model.addAttribute("groupeForm", groupeForm);

        return "addGroupe";
    }

    @PostMapping(value = { "/addGroupe"})
    public String addGroupe(Model model, @ModelAttribute("GroupeForm") GroupeForm groupeForm) {
        Groupe groupe;
        if(groupeService.findById(groupeForm.getIdG()).isPresent()){
            // groupe already existing : update
            groupe = groupeService.findById(groupeForm.getIdG()).get();
            groupe.setNom(groupeForm.getNom());
        } else {
            // groupe not existing : create
            groupe=new Groupe(groupeForm.getNom());
        }
        groupeService.saveGroupe(groupe);
        return "redirect:/listGroupes";

    }

    @GetMapping(value = "/showGroupeUpdateForm/{id}")
    public String showGroupeUpdateForm(Model model, @PathVariable(value = "id") long id){
        GroupeForm groupeForm = new GroupeForm(groupeService.findById(id).get().getNom(),id);
        model.addAttribute("groupeForm",groupeForm);
        return "updateGroupe";
    }

    @GetMapping(value = {"/deleteGroupe/{id}"})
    public String deleteGroupe(Model model, @PathVariable(value = "id") long id){
        groupeService.deleteGroupe(id);
        return "redirect:/listGroupes";
    }
}